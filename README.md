## EP2 - OO 2016/2 (UnB - Gama)

Este projeto consiste em uma aplicação desktop para um restaurante, utilizando a técnologia Java Swing.

-UML disponivel na pasta do projeto.


## Para Executar o programa


--> Primeiramente deve se utilizar uma IDE como : Eclipse-IDE.

--> Depois abrir a pasta do arquivo na IDE e Executar o projeto.


## Abrindo e utilizando o programa / teste do programa


--> O usuário irá se deparar com uma tela de login , onde o usuario deverá digitar o usuário presente entre parenteses, referente ao usuário de funcionário.

--> O usuário irá se deparar com uma tela de senha , onde o usuario deverá digitar a senha presente entre parenteses, referente a senha do usuário de funcionário.


--> Logo após fazer o login abrirá a tela principal onde há várias abas, como:

	-->Produtos

	-->Clientes

	-->Pedidos


--> Dentro de tais abas, temos as opções:

	--> Cadastrar(Produtos) :: 1

	--> Listar(Produtos) :: 2

	--> Cadastrar(Clientes) :: 3

	--> Listar(Clientes) :: 4

	--> Registrar Pedido :: 5

	--> Fechar Conta :: 6


--> A seguir, o procedimento e resultado de uso de cada opção:

	::1 - Digite o código do produto - Insira o nome do produto - Insira o valor do produto - Insira a quantidade mínima em estoque --> Mensagem de produto cadastrado com as informações inseridas.

	::2 - Ao selecionar a opção, mostra uma tela com os nomes dos produtos cadastrados, código, quantidade em estoque e quantidade mínima

	::3 - Insira o nome do cliente - Insira o RG do cliente --> Mensagem de cliente cadastrado com as informações inseridas.

	::4 - selecionar a opção, mostra uma tela com os nomes dos clientes cadastrados e os RGs dos mesmos. Caso não encontre o RG do cliente, mostra uma mensagem de erro.

	::5 - Insira o RG do cliente - código do produto - confirme se quer ou não adicionar mais algo ao pedido - adicione uma observação.

	::6 - Insira o RG do cliente --> Tela com a descrição do cliente, produtos consumidos, preço unitário e preço total. Após selecionada a opção de pagamento, encerra caso a mesma seja cartão de crédito, com uma tela de pagamento efetuado. Se for dinheiro, pede a quantidade paga e retorna o troco a ser dado ao cliente.




