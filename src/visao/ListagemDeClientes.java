package visao;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import modelo.Cliente;

public class ListagemDeClientes extends JPanel {

  
	private static final long serialVersionUID = 1L;

JList<String> lista;

  DefaultListModel<String> modelo;

  public ListagemDeClientes(ArrayList<Cliente> clientes) {
	setLayout(new BorderLayout());
    modelo = new DefaultListModel<String>();
    lista = new JList<String>(modelo);
    JScrollPane pane = new JScrollPane(lista);
    
    modelo.addElement("NOME :: RG");
    for(Cliente cliente: clientes){
		modelo.addElement(cliente.getNome()+" :: "+cliente.getRg());
    }


    add(pane, BorderLayout.NORTH);

  } 
}