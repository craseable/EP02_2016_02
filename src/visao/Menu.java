package visao;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.WindowConstants;

import controle.Cadastro;
public class Menu extends JFrame{
	
	
	
	private static final long serialVersionUID = 1L;
	private JMenuBar BarraMenu = null;
	private JMenu menuArquivo = null;
	private JMenu menuClientes = null;
	private JMenu menuPedido = null;
	private JMenuItem setProduto = null;
	private JMenuItem listaProduto = null;
	private JMenuItem setCliente = null;
	private JMenuItem listaCliente = null;
	private JMenuItem fazerPedido = null;
	private JMenuItem fecharConta = null;
	
	
	Cadastro cadastro = new Cadastro();
	public Menu() {
		super();
	}
	public void inicializar() throws IOException{
		JFrame frame = new JFrame();
		BufferedImage imagem = null;
		String fonte = "fome2.png";
        imagem = ImageIO.read(new File(fonte));
        JLabel label = new JLabel(new ImageIcon(imagem));
        
		frame.setTitle("Tô com fome, quero mais!");
		frame.setJMenuBar(getBarraMenu());
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.getContentPane().add(label);
		frame.pack();
		frame.setSize(1280,410);
		frame.setVisible(true);
		frame.add(new JLabel(new ImageIcon("../../fome1.png")));
		
		//Caso este item seja escolhido..
		setProduto.addActionListener(new java.awt.event.ActionListener() {
		    public void actionPerformed(java.awt.event.ActionEvent evt) {
		        try {
					cadastro.deposito(evt);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
		
		//Caso este item seja escolhido..
		listaProduto.addActionListener(new java.awt.event.ActionListener() {
		    public void actionPerformed(java.awt.event.ActionEvent evt) {
		        cadastro.listaProdutos(evt);
		    }
		});
		
		//Caso este item seja escolhido..
		setCliente.addActionListener(new java.awt.event.ActionListener() {
		    public void actionPerformed(java.awt.event.ActionEvent evt) {
		        cadastro.storageClient(evt);
		    }
		});
		
		//Caso este item seja escolhido..
		listaCliente.addActionListener(new java.awt.event.ActionListener() {
		    public void actionPerformed(java.awt.event.ActionEvent evt) {
		        cadastro.listaClientes(evt);
		    }
		});
		
		//Caso este item seja escolhido..
		fazerPedido.addActionListener(new java.awt.event.ActionListener() {
		    public void actionPerformed(java.awt.event.ActionEvent evt) {
		        cadastro.pedido(evt);
		    }
		});
		
		//Caso este item seja escolhido..
		fecharConta.addActionListener(new java.awt.event.ActionListener() {
		    public void actionPerformed(java.awt.event.ActionEvent evt) {
		        cadastro.fecharConta(evt);
		    }
		});
		
	}
	//Construindo a barra do menu
	public JMenuBar getBarraMenu() {
		if (BarraMenu == null){
			BarraMenu = new JMenuBar();
			BarraMenu.add(getMenuArquivo());
			BarraMenu.add(getMenuClientes());
			BarraMenu.add(getMenuPedidos());
		}
		return BarraMenu;
	}
	public JMenu getMenuArquivo() {
		if (menuArquivo == null){
			menuArquivo = new JMenu();
			menuArquivo.setText("Produtos");
			menuArquivo.add(getMenuSetProduto());
			menuArquivo.add(getMenuListaProduto());
		}
		return menuArquivo;
	}
	
	public JMenu getMenuClientes() {
		if (menuClientes == null){
			menuClientes = new JMenu();
			menuClientes.setText("Clientes");
			menuClientes.add(getMenuSetCliente());
			menuClientes.add(getMenuListaCliente());
		}
		return menuClientes;
	}
	
	public JMenu getMenuPedidos() {
		if (menuPedido == null){
			menuPedido = new JMenu();
			menuPedido.setText("Pedidos");
			menuPedido.add(getMenuFazerPedido());
			menuPedido.add(getMenuFecharConta());
		}
		return menuPedido;
	}
	
	public JMenuItem getMenuSetCliente() {
		if (setCliente == null){
			setCliente = new JMenuItem();
			setCliente.setText("Cadastrar");
		}
		return setCliente;
	}
	
	public JMenuItem getMenuListaCliente() {
		if (listaCliente == null){
			listaCliente = new JMenuItem();
			listaCliente.setText("Listar");
		}
		return listaCliente;
	}
	
	
	public JMenuItem getMenuSetProduto() {
		if (setProduto == null){
			setProduto = new JMenuItem();
			setProduto.setText("Cadastrar");
		}
		return setProduto;
	}
	public JMenuItem getMenuListaProduto() {
		if (listaProduto == null){
			listaProduto = new JMenuItem();
			listaProduto.setText("Listar");
		}
		return listaProduto;
	}
	
	public JMenuItem getMenuFazerPedido() {
		if (fazerPedido == null){
			fazerPedido = new JMenuItem();
			fazerPedido.setText("Registrar pedido");
		}
		return fazerPedido;
	}
	
	public JMenuItem getMenuFecharConta() {
		if (fecharConta == null){
			fecharConta = new JMenuItem();
			fecharConta.setText("Fechar conta");
		}
		return fecharConta;
	}
	
}