package visao;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import controle.Pag_Dinheiro;
import controle.Pag_Credito;
import modelo.Cliente;
import modelo.Produto;

public class ListagemConta extends JPanel {

  
	private static final long serialVersionUID = 1L;

JList<String> lista;

  DefaultListModel<String> modelo;

  public ListagemConta(ArrayList<Produto> produtos, Cliente cliente, final float valorTotal, final JFrame frame) {
	setLayout(new BorderLayout());
    modelo = new DefaultListModel<String>();
    final Pag_Dinheiro dinheiro = new Pag_Dinheiro();
    final Pag_Credito credito = new Pag_Credito();
    lista = new JList<String>(modelo);
    JButton addButton = new JButton("Dinheiro");
    JButton addButtonCard = new JButton("Cartão");
    JScrollPane pane = new JScrollPane(lista);
    modelo.addElement("Cliente: "+cliente.getNome());
    modelo.addElement("PRODUTO :: PREÇO");
    for(Produto produto: produtos){
		modelo.addElement(produto.getNome()+" :: "+produto.getPreco());
    }
    
    modelo.addElement("Total: "+valorTotal);
    
    addButton.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          while(!dinheiro.pag(valorTotal));
          
          frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
 	
        }
      });
    addButtonCard.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          credito.pag(valorTotal);
          frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
 	
        }
      });

    add(pane, BorderLayout.NORTH);
    add(addButton, BorderLayout.WEST);
    add(addButtonCard, BorderLayout.EAST);

  } 
}