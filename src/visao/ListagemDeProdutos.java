package visao;

import java.awt.BorderLayout;

import java.util.ArrayList;

import javax.swing.DefaultListModel;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;


import modelo.Produto;
import modelo.Estoque;

public class ListagemDeProdutos extends JPanel {

  
	private static final long serialVersionUID = 1L;

JList<String> lista;

  DefaultListModel<String> modelo;

  public ListagemDeProdutos(ArrayList<Produto> produtos, ArrayList<Estoque> listaEstoque) {
	setLayout(new BorderLayout());
    modelo = new DefaultListModel<String>();
    lista = new JList<String>(modelo);
    JScrollPane pane = new JScrollPane(lista);
    
    modelo.addElement("NOME :: CODIGO :: QUANTIDADE :: MINIMO");
    for(Produto produto: produtos){
    	for(Estoque estoque: listaEstoque){
    		if(estoque.getCod() == produto.getCodigo()){
    				modelo.addElement(produto.getNome()+" :: "+ estoque.getCod()+" :: "+ estoque.getQuantidadeEstoque()+" :: "+estoque.getQuantidadeMinima());
    		}
    	}
    	
    }


    add(pane, BorderLayout.NORTH);

  } 
}