package controle;

import javax.swing.JOptionPane;

public class Pag_Dinheiro extends Pagamento{

	public boolean pag(float valorTotal){
		float valor = Float.parseFloat(JOptionPane.showInputDialog("Pagamento em dinheiro. Entre com o valor pago:"));
		if(valorTotal < valor){
			JOptionPane.showMessageDialog(null, "O troco é: " +(valor-valorTotal));
			return true;
		}else{
			JOptionPane.showMessageDialog(null, "Valor insuficiente para pagamento da conta!");
			return false;
		}
	}

}
