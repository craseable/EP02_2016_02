package controle;


import java.io.IOException;

import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.Cliente;
import modelo.Funcionario;
import modelo.Pedidos;
import modelo.Produto;
import modelo.Estoque;
import visao.ListagemDeClientes;
import visao.ListagemConta;
import visao.ListagemDeProdutos;

public class Cadastro {
	
	//Registro de todos os cadastros realizados
	ArrayList<Cliente> listaClientes = new ArrayList<Cliente>();
	ArrayList<Funcionario> listaEmpregados = new ArrayList<Funcionario>();
	ArrayList<Estoque> listaEstoque = new ArrayList<Estoque>();
	ArrayList<Produto> listaProdutos = new ArrayList<Produto>();
	
	public void deposito(java.awt.event.ActionEvent evt) throws IOException {
		String codigo;
		String nome;
		String preco;
		int quantidadeEstoque;
		boolean estoqueValido = false;
		
		Produto produto = new Produto();
	    
	    codigo = JOptionPane.showInputDialog("Insira o código do produto:");
	    if(codigo == null){ //Se o usuário clicar em cancelar..
	    	return;
	    }else{
	    	produto.setCodigo(Integer.parseInt(codigo));
	    }
	    
	    for(Estoque estoque: listaEstoque){
	    	if(estoque.getCod() == produto.getCodigo()){
	    		estoqueValido = true;
	    		quantidadeEstoque = estoque.getQuantidadeEstoque();
	    		estoque.setQuantidadeEstoque(quantidadeEstoque+1);
	    		return;
	    	}
	    }
	    
	    nome = JOptionPane.showInputDialog("Insira o nome do produto:");
	    if(nome == null){ //Se o usuário cancelar
	    	return;
	    }else{
	    	produto.setNome(nome);
	    }
	    
	    preco = JOptionPane.showInputDialog("Insira o valor do produto:");
	    if(preco == null){ //Se o usuário cancelar
	    	return;
	    }else{
	    	produto.setPreco(Float.parseFloat(preco));
	    }
	    
	    //Depois que o usuário entrar com os dados do produto, preciso atualizar o estoque
	    
	    if(!estoqueValido){ //Caso o estoque deste produto ainda n tenha sido criado:
	    	Estoque estoque = new Estoque();
	    	estoque.setCod(produto.getCodigo());
	    	int minEstoque = Integer.parseInt(JOptionPane.showInputDialog("Qual a quantidade mínima necessária no estoque?"));
	    	estoque.setQuantidadeMinima(minEstoque);
	    	estoque.setQuantidadeEstoque(1);
	    	JOptionPane.showMessageDialog(null, "Produto Cadastrado!\nNome:"+ produto.getNome()+"\nCodigo:" + produto.getCodigo() + "\nQuantidade"+estoque.getQuantidadeEstoque() + "\nMinimo exigido: "+ estoque.getQuantidadeMinima());
		    listaEstoque.add(estoque);
	    }
	    listaProdutos.add(produto);
	   
	}
	
	public void listaProdutos(java.awt.event.ActionEvent evt) {
		JFrame frame = new JFrame("Listagem de produtos");
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	    frame.setContentPane(new ListagemDeProdutos(listaProdutos, listaEstoque));
	    frame.setSize(260, 200);
	    frame.setVisible(true);  
	}
	
	public void listaClientes(java.awt.event.ActionEvent evt) {
		JFrame frame = new JFrame("Listagem de clientes");
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	    frame.setContentPane(new ListagemDeClientes(listaClientes));
	    frame.setSize(260, 200);
	    frame.setVisible(true);  
	    
	}
	
	public void mudarMinimoEstoque(java.awt.event.ActionEvent evt) throws IOException{
		int minimo;
		int codigo;
		codigo = Integer.parseInt(JOptionPane.showInputDialog("Digite o codigo do produto:"));
		minimo = Integer.parseInt(JOptionPane.showInputDialog("Digite o mínimo atualizado:"));
		for(Estoque estoque: listaEstoque){
			if(estoque.getCod() == codigo){
				estoque.setQuantidadeMinima(minimo);
			}
		}
	}
	
	//Clientes:
	public void storageClient(java.awt.event.ActionEvent evt){
		Cliente cliente = new Cliente();
		String nome;
		String rg;
		nome = JOptionPane.showInputDialog("Nome do cliente:");
		rg = JOptionPane.showInputDialog("RG do cliente:");
		cliente.setNome(nome);
		cliente.setRg(rg);
		JOptionPane.showMessageDialog(null, "Cliente cadastrado com sucesso!\nNome: "+ cliente.getNome()+"\nRG: "+cliente.getRg());
		listaClientes.add(cliente);
	}
	
	//Pedidos:
	public void pedido(java.awt.event.ActionEvent evt){
		String codigoProduto;
		String rg;
		int opcao;
		float valorTotal = 0;
		boolean achar = false;
		boolean acharCliente = false;
		ArrayList<Produto> produtos = new ArrayList<Produto>();
		ArrayList<Pedidos> pedidoCliente = new ArrayList<Pedidos>();
		Pedidos pedido = new Pedidos();
		rg = JOptionPane.showInputDialog("Entre com o RG do cliente:");
		
		for(Cliente cliente: listaClientes){
			if(cliente.getRg().equals(rg)){
				if(cliente.getPedidos() != null){
					pedidoCliente = cliente.getPedidos();
				}
				do{
					codigoProduto = JOptionPane.showInputDialog("Entre com o código do produto:");
					for(Produto produto: listaProdutos){
						if(produto.getCodigo() == Integer.parseInt(codigoProduto)){
							for(Estoque estoque : listaEstoque){
								if(estoque.getCod() == produto.getCodigo()){
									if(estoque.getQuantidadeEstoque() < estoque.getQuantidadeMinima()){
										JOptionPane.showMessageDialog(null, "Produto indisponível, em falta no estoque!");
										return;
									}
								}
							}
							produtos.add(produto);
							achar = true;
							valorTotal = valorTotal + produto.getPreco();
						}
					}
					if(!achar){
						JOptionPane.showMessageDialog(null, "Erro! Produto não cadastrado!");
					}
					acharCliente = true;
					opcao = JOptionPane.showConfirmDialog(null, "Deseja inserir algum outro produto no pedido?","", JOptionPane.YES_NO_OPTION);
				}while(opcao == JOptionPane.YES_OPTION);
				
				pedido.setObservacao(JOptionPane.showInputDialog("Observação:"));
				pedido.setData(Calendar.getInstance());
				pedido.setProdutos(produtos);
				pedido.setValorTotal(valorTotal);
				pedidoCliente.add(pedido);
				cliente.setPedidos(pedidoCliente);
				
			}
		}
		
		if(!acharCliente){
			JOptionPane.showMessageDialog(null, "Erro! Cliente não cadastrado!");
		}
		
		
		
	}
	
	public void fecharConta(java.awt.event.ActionEvent evt){
		String rg;
		float valorTotal = 0;
		ArrayList<Pedidos> pedidosCliente = new ArrayList<Pedidos>();
		ArrayList<Produto> produtosPedido = new ArrayList<Produto>();
		Cliente pessoa = new Cliente();
		
		rg = JOptionPane.showInputDialog("Entre com o RG do cliente:");
		for(Cliente cliente : listaClientes){
			if(cliente.getRg().equals(rg)){ //Se encontrar o cliente
				pedidosCliente = cliente.getPedidos();
				pessoa = cliente;
				for(Pedidos pedido : pedidosCliente){
					valorTotal = valorTotal + pedido.getValorTotal();
					produtosPedido.addAll(pedido.getProdutos());
				}
			}
		}
		JFrame frame = new JFrame("Conta:");
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	    frame.setContentPane(new ListagemConta(produtosPedido, pessoa, valorTotal, frame));
	    frame.setSize(260, 200);
	    frame.setVisible(true);
	}
 
}