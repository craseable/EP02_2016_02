package modelo;

public class Pessoa{
	
	
	private String nome;
	private String rg;
	
	public Pessoa(){

	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	@Override
	public int hashCode() {
		final int primo = 31;
		int resultado = 1;
		resultado = primo * resultado + ((rg == null) ? 0 : rg.hashCode());
		return resultado;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pessoa outraPessoa = (Pessoa) obj;
		if (rg == null) {
			if (outraPessoa.rg != null)
				return false;
		} else if (!rg.equals(outraPessoa.rg))
			return false;
		return true;
	}
	
}
