package modelo;

public class Estoque {
	private Integer quantidade;
	private Integer quantidadeMin;
	private Integer cod;
	
	public Estoque(){
		
	}

	public Integer getQuantidadeEstoque() {
		return quantidade;
	}

	public void setQuantidadeEstoque(Integer quantidade) {
		this.quantidade = quantidade;
	}


	public Integer getQuantidadeMinima() {
		return quantidadeMin;
	}

	public void setQuantidadeMinima(Integer quantidadeMin) {
		this.quantidadeMin = quantidadeMin;
	}

	public Integer getCod() {
		return cod;
	}

	public void setCod(Integer cod) {
		this.cod = cod;
	}
	
	
	
}
