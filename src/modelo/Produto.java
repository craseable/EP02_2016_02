package modelo;

public class Produto {
	private String nome;
	private Integer codigo;
	private float preco;
	
	public Produto(){
		
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public float getPreco() {
		return preco;
	}

	public void setPreco(float preco) {
		this.preco = preco;
	}

	@Override
	public int hashCode() {
		final int primo = 31;
		int resultado = 1;
		resultado = primo * resultado + ((codigo == null) ? 0 : codigo.hashCode());
		return resultado;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto outroProduto = (Produto) obj;
		if (codigo == null) {
			if (outroProduto.codigo != null)
				return false;
		} else if (!codigo.equals(outroProduto.codigo))
			return false;
		return true;
	}
	
	
	
}
